#!/bin/bash

# https://gist.github.com/Jamesits/944ab9a484ad34522f6c4bce8d085132
# Captures every IP-CIDR notation in HTML file
# then convert it to RouterOS IP List format
# Usage: convert.sh source-file.html List-Name
# Then use /import file-name=List-Name.scr to 
# import this list on RouterOS
# You can save bgp.he.net search result as HTML
# then use this script to convert.

set -eu

if [ "$#" -ne 2 ] || ! [ -f "$1" ]; then
  echo "Usage: $0 source-file.html List-Name" >&2
  exit 1
fi

cat > $2.scr <<EOF
/ip firewall address-list
remove [/ip firewall address-list find list=$2]
EOF
egrep -oe "(\d{2,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,3})" "$1" | sed "s/^/add address=/g" | sed "s/\$/ list=$2/g" | awk '!a[$0]++' >> $2.scr
